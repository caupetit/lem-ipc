/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   target.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/01 14:59:55 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:10:15 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemipc.h"

void		set_target(t_env *e, int x, int y)
{
	e->target = y * WIDTH + x;
}

void		get_team_pos(t_env *e, int *board)
{
	int		i;
	int		tmp;
	int		nb;

	tmp = 0;
	nb = 0;
	i = -1;
	while (++i < MAX_SIZE)
	{
		if (board[i] == e->team)
		{
			tmp += i;
			nb++;
		}
	}
	e->t_pos = tmp / nb;
}

void		get_target(t_env *e, int *board)
{
	int		i;
	double	dist;
	double	tmp;
	int		target;

	dist = MAX_SIZE;
	target = 0;
	i = -1;
	while (++i < MAX_SIZE)
	{
		if (board[i] != 0 && board[i] != e->team)
		{
			tmp = get_dist(e->t_pos, board[i]);
			if (dist > tmp)
			{
				dist = tmp;
				target = i;
			}
		}
	}
	e->target = target;
}
