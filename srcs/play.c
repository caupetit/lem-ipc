/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   play.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/31 14:06:47 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:25:41 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemipc.h"
#include "libft.h"

void		play(t_env *e, t_data *d)
{
	int		*board;
	int		i;
	int		j;

	i = e->pos % WIDTH;
	j = e->pos / WIDTH;
	board = (int *)((void *)d + sizeof(t_data));
	if (is_alive(e, board, i, j))
		move(e, board, j);
	else
	{
		e->alive = 0;
		board[e->pos] = 0;
		d->players -= 1;
		if (!team_exist(board, e->team))
		{
			ft_printf("Team %d loose\n", e->team);
			d->teams -= 1;
		}
	}
}
