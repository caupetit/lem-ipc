/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dir.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/01 14:26:13 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 19:34:45 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "lemipc.h"

double		get_dist(int a, int b)
{
	double	x;
	double	y;

	x = (double)(a % WIDTH - b % WIDTH);
	y = (double)(a / WIDTH - b / WIDTH);
	return (sqrt(x * x + y * y));
}

void		set_dir(t_env *e, int *dir, int *moves)
{
	int		i;
	double	dist;
	double	tmp;

	dist = MAX_SIZE;
	i = -1;
	while (++i < 4)
	{
		tmp = get_dist(e->pos + moves[i], e->target);
		if (dist > tmp)
		{
			dist = tmp;
			*dir = i;
		}
	}
}

int			on_target(t_env *e)
{
	if (e->pos == e->target)
		return (1);
	if (e->pos == e->target - 1 || e->pos == e->target + 1)
		return (1);
	if (e->pos == e->target - WIDTH || e->pos == e->target + WIDTH)
		return (1);
	if (e->pos == e->target - WIDTH - 1 || e->pos == e->target + WIDTH + 1)
		return (1);
	if (e->pos == e->target - WIDTH + 1 || e->pos == e->target + WIDTH - 1)
		return (1);
	return (0);
}

int			get_dir(t_env *e, int *moves, int j, int *board)
{
	int		dir;

	if (on_target(e))
	{
		dir = rand_int(0, 4);
		while (!move_ok(j, e->pos, moves[dir]))
			dir = rand_int(0, 4);
	}
	else
	{
		set_dir(e, &dir, moves);
		if (!move_ok(j, e->pos, moves[dir]) || board[e->pos + moves[dir]] != 0)
			dir = rand_int(0, 4);
	}
	return (dir);
}
