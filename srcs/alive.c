/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alive.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/01 14:35:59 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:22:10 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemipc.h"

void		count_players(t_env *e, int *board, int player, int *nb)
{
	int		k;
	int		l;
	int		i;
	int		j;

	i = e->pos % WIDTH - 1;
	j = e->pos / WIDTH - 1;
	l = -1;
	while (++l < 3)
	{
		k = -1;
		while (++k < 3)
		{
			if (i >= 0 && i < WIDTH && j >= 0 && j < HEIGHT)
			{
				if (board[j * WIDTH + i] == player)
					*nb += 1;
			}
			i++;
		}
		i -= 3;
		j++;
	}
}

int			check_pos(t_env *e, int *board, int i, int j)
{
	int		pos;
	int		nb;

	nb = 0;
	if (i >= 0 && i < WIDTH && j >= 0 && j < HEIGHT)
	{
		pos = j * WIDTH + i;
		if (board[pos] != 0 && board[pos] != e->team)
		{
			count_players(e, board, board[pos], &nb);
			if (nb > 1)
				return (0);
		}
	}
	return (1);
}

int			is_alive(t_env *e, int *board, int i, int j)
{
	int		k;
	int		l;

	i -= 1;
	j -= 1;
	l = -1;
	while (++l < 3)
	{
		k = -1;
		while (++k < 3)
		{
			if (!check_pos(e, board, i, j))
				return (0);
			i++;
		}
		i -= 3;
		j++;
	}
	return (1);
}
