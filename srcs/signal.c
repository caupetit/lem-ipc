/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/21 17:04:19 by caupetit          #+#    #+#             */
/*   Updated: 2014/05/31 19:00:03 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <signal.h>
#include "lemipc.h"
#include "libft.h"

void		die_sig(int sig)
{
	t_env	*env;

	(void)sig;
	env = get_env();
	rem_sem(env);
	shm_dt(env);
	ft_printf("Good Bye\n");
	exit(0);
}

void		sig_enable(void)
{
	signal(SIGINT, &die_sig);
	signal(SIGKILL, &die_sig);
	signal(SIGQUIT, &die_sig);
	signal(SIGABRT, &die_sig);
	signal(SIGTERM, &die_sig);
}
