/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/21 17:04:19 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:33:53 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lemipc.h"
#include "libft.h"

int			time_to_die(int time)
{
	static int	i;

	if (i++ < time)
		return (0);
	return (1);
}

t_env		*get_env(void)
{
	static t_env	*env;

	if (!env)
	{
		testv(env = (t_env *)malloc(sizeof(t_env)), NULL, " get_env()");
		ft_bzero(env, sizeof(t_env));
	}
	return (env);
}

int			main(int ac, char **av)
{
	t_env	*e;

	sig_enable();
	e = get_env();
	init_env(e, ac, av);
	init_game(e);
	game_loop(e);
	rem_sem(e);
	shm_dt(e);
	return (0);
}
