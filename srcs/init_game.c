/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_game.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/31 17:20:46 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 15:03:57 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "lemipc.h"
#include "libft.h"

int			team_exist(int *board, int team)
{
	int		i;

	i = -1;
	while (++i < MAX_SIZE)
	{
		if (board[i] == team)
			return (1);
	}
	return (0);
}

static void	create_player(t_env *e, int *board)
{
	int		i;

	i = rand_int(0, MAX_SIZE);
	while (board[i] != 0)
		i = rand_int(0, MAX_SIZE);
	board[i] = e->team;
	set_target(e, WIDTH / 2, HEIGHT / 2);
	e->alive = 1;
	e->pos = i;
}

static int	allow_player(t_env *e, t_data *d, int *board)
{
	if (d->players + 1 >= MAX_SIZE)
		return (0);
	if (!team_exist((int *)board, e->team))
		d->teams += 1;
	d->players += 1;
	if (d->players > 2 && d->teams > 1)
		e->start = 1;
	return (1);
}

void		init_game(t_env *e)
{
	t_data	*d;
	int		*board;
	int		err;

	err = 0;
	e->end = 0;
	srand(time(NULL));
	sem_op(e, SEM_ASK);
	d = e->shm;
	board = (int *)((void *)d + sizeof(t_data));
	if (allow_player(e, d, board))
		create_player(e, board);
	else
		err = 1;
	sem_op(e, SEM_FREE);
	if (err)
		die(error(1, PLAYERS), NULL);
}
