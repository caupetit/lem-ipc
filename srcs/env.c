/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/21 17:04:19 by caupetit          #+#    #+#             */
/*   Updated: 2014/05/23 19:01:02 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include "lemipc.h"
#include "libft.h"

void		get_key(t_env *e, char *pathname)
{
	test((e->key = ftok(pathname, 'p')), -1, " ftok()");
}

void		get_team(t_env *e, char *team)
{
	e->team = ft_atoi(team);
	if (e->team < 1)
		die(error(1, TEAM), NULL);
}

void		init_env(t_env *e, int ac, char **av)
{
	if (ac != 2)
		die(error(1, USAGE), NULL);
	get_key(e, av[0]);
	get_team(e, av[1]);
	get_shm(e);
	get_sem(e);
}
