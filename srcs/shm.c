/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shm.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/21 17:04:19 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:26:33 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "lemipc.h"
#include "libft.h"

void		get_shm(t_env *e)
{
	int		size;

	size = (sizeof(int) * MAX_SIZE + sizeof(t_data));
	test((e->shmid = shmget(e->key, size, 0666 | IPC_CREAT)), -1, " shmget()");
	testv((e->shm = shmat(e->shmid, (void *)0, 0)), (void *)-1, " shmat()");
}

void		rem_shm(t_env *e)
{
	test(shmctl(e->shmid, IPC_RMID, 0), -1, " shmctl()");
}

void		shm_dt(t_env *e)
{
	test(shmdt(e->shm), -1, " shmdt()");
}
