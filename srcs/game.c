/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/21 17:04:19 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:23:55 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "lemipc.h"
#include "libft.h"

void		start_game(t_env *e, t_data *d)
{
	if (d->players > 2 && d->teams > 1)
		e->start = 1;
}

int			rand_int(int min, int max)
{
	return (rand() % (max + min) + min);
}

int			team_win(t_env *e)
{
	static int	test;
	t_data		*d;

	if (e->end)
		return (1);
	if (test)
		e->end = 1;
	test = 0;
	sem_op(e, SEM_ASK);
	d = e->shm;
	if (e->start && d->teams < 2)
		test = 1;
	sem_op(e, SEM_FREE);
	return (0);
}

void		game_loop(t_env *e)
{
	static int	i;
	t_data		*d;

	while (e->alive && !time_to_die(60) && !team_win(e))
	{
		i++;
		sem_op(e, SEM_ASK);
		d = e->shm;
		put_scene(e);
		start_game(e, d);
		ft_printf("time: %d\n", i);
		if (e->start)
			play(e, d);
		sem_op(e, SEM_FREE);
		usleep(TIME);
	}
	put_end_states(e);
}
