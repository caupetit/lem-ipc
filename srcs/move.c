/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/31 15:43:39 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:25:11 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemipc.h"
#include "libft.h"

int			move_ok(int j, int pos, int move)
{
	int		new;

	new = pos + move;
	if (ABS(move) != 1 && (new < 0 || new / WIDTH >= HEIGHT))
		return (0);
	if (ABS(move) == 1 && new / WIDTH != j)
		return (0);
	return (1);
}

void		move(t_env *e, int *board, int j)
{
	int			dir;
	static int	moves[4] =

	{
	-1,
	1,
	-WIDTH,
	WIDTH
	};
	get_team_pos(e, board);
	get_target(e, board);
	dir = get_dir(e, moves, j, board);
	if (move_ok(j, e->pos, moves[dir]) && board[e->pos + moves[dir]] == 0)
	{
		board[e->pos] = 0;
		e->pos += moves[dir];
		board[e->pos] = e->team;
	}
}
