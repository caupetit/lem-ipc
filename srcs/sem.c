/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sem.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/21 17:04:19 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:36:05 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "lemipc.h"

void		get_sem(t_env *e)
{
	test((e->semid = semget(e->key, 1, IPC_CREAT | 0666)), -1, " semget");
	test(semctl(e->semid, 0, SETVAL, 1), -1, "semctl(SET_VAL)");
}

void		rem_sem(t_env *e)
{
	int		*board;
	int		del;
	t_data	*d;

	del = 0;
	sem_op(e, SEM_ASK);
	d = e->shm;
	if (e->alive)
	{
		board = (int *)((void *)d + sizeof(t_data));
		board[e->pos] = 0;
		d->players -= 1;
	}
	if (d->players == 0)
		del = 1;
	sem_op(e, SEM_FREE);
	if (del)
	{
		rem_shm(e);
		test(semctl(e->semid, 0, IPC_RMID, 0), -1, " semctl(IPC_RMID)");
	}
}

void		sem_op(t_env *e, int ope)
{
	struct sembuf	op;

	op.sem_num = 0;
	op.sem_op = ope;
	op.sem_flg = 0;
	test(semop(e->semid, &op, 1), -1, " semop()");
}
