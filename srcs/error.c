/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/01 20:22:40 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:23:30 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <stdlib.h>
#include "lemipc.h"
#include "libft.h"

int			error(int ret, char *error)
{
	ft_putstr_fd("Error: ", 2);
	if (error)
		ft_putstr_fd(error, 2);
	return (ret);
}

void		die(int error, char *where)
{
	if (where)
		ft_putendl_fd(where, 2);
	else
		ft_putstr_fd("\n", 2);
	exit(error);
}

int			test(int ret, int err, char *where)
{
	if (ret == err)
		die(error(ret, strerror(errno)), where);
	return (ret);
}

void		*testv(void *ret, void *err, char *where)
{
	if (ret == err)
		die(error(1, strerror(errno)), where);
	return (ret);
}
