/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/31 13:53:54 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:25:53 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"
#include "lemipc.h"

void		put_end_states(t_env *e)
{
	if (!e->alive)
		ft_printf("Client died\n");
	else if (e->end)
		ft_printf("Team %d Win !!!!!\n", e->team);
	else
		ft_printf("Max time reached\n");
}

static void	put_limit(void)
{
	int		i;

	i = -1;
	while (++i < WIDTH)
		ft_printf("--");
	ft_printf("---\n");
}

void		put_scene(t_env *e)
{
	t_data	*d;
	int		*board;
	int		i;

	d = e->shm;
	board = (int *)((void *)d + sizeof(t_data));
	write(1, "\033[2J\033[r", 8);
	put_limit();
	i = -1;
	while (++i < MAX_SIZE)
	{
		if (i != 0 && i % WIDTH == 0)
			ft_printf(" |\n");
		if (i % WIDTH == 0)
			ft_printf("|");
		if (board[i] != 0 && i == e->pos)
			ft_printf ("\033[34;01m %d\033[00m", board[i]);
		else if (board[i] != 0)
			ft_printf(" %d", board[i]);
		else
			ft_printf("  ");
	}
	ft_printf(" |\n");
	put_limit();
}
