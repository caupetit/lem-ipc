# README #

Lem-IPC makes process communicate with SHM and semaphores.

Little AI fight where each process is a player. When two players from a team surround another team player, he dies.

Game ends when only one team is alive.

* * *

# How to use ? #

Simply use:

* make

* ./lemipc <team number>

Repeat last command for each player you want to add to the match. Game starts when at least 3 players and 2 team have spawn on map.

* * *

# Screenshots #

![Lem-IPC.jpg](https://bitbucket.org/repo/5Ab6BG/images/1007963634-Lem-IPC.jpg)