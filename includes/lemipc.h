/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemipc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/21 17:04:19 by caupetit          #+#    #+#             */
/*   Updated: 2014/06/01 20:30:51 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIPC_H
# define LEMIPC_H

# include "sys/types.h"

# define USAGE		"Usage: ./lemipc <team>"
# define TEAM		"Invalid team number"
# define PLAYERS	"Game board full"

# define TIME		1000000

# define SEM_ASK	-1
# define SEM_FREE	1

# define MSG_SIZE	40

# define WIDTH		20
# define HEIGHT		20
# define MAX_SIZE	WIDTH * HEIGHT

# define ABS(a)		((a >= 0) ? a : -a)

typedef struct		s_data
{
	int		players;
	int		teams;
}					t_data;

typedef struct		s_env
{
	key_t	key;
	void	*shm;
	int		shmid;
	int		msqid;
	int		semid;
	int		alive;
	int		pos;
	int		team;
	int		t_pos;
	int		start;
	int		end;
	int		target;
}					t_env;

int					time_to_die(int time);
t_env				*get_env(void);

/*
**	signal.c
*/
void				die_sig(int sig);
void				sig_enable(void);

/*
**	env.c
*/
void				get_key(t_env *e, char *pathname);
void				get_team(t_env *e, char *team);
void				init_env(t_env *e, int ac, char **av);

/*
**	shm.c
*/
void				get_shm(t_env *e);
void				rem_shm(t_env *e);
void				shm_dt(t_env *e);

/*
**	sem.h
*/
void				get_sem(t_env *e);
void				rem_sem(t_env *e);
void				sem_op(t_env *e, int ope);

/*
**	init_game.c
*/
void				init_game(t_env *e);
int					team_exist(int *board, int team);

/*
**	game.c
*/
void				start_game(t_env *e, t_data *d);
int					rand_int(int min, int max);
void				game_loop(t_env *e);

/*
**	target.c
*/
void				get_team_pos(t_env *e, int *board);
void				get_target(t_env *e, int *board);
void				set_target(t_env *e, int x, int y);

/*
**	play.c
*/
void				play(t_env *e, t_data *d);

/*
**	alive.c
*/
void				count_players(t_env *e, int *board, int player, int *nb);
int					check_pos(t_env *e, int *board, int i, int j);
int					is_alive(t_env *e, int *board, int i, int j);

/*
**	move.c
*/
int					move_ok(int j, int pos, int move);
void				move(t_env *e, int *board, int j);

/*
**	dir.c
*/
double				get_dist(int a, int b);
void				set_dir(t_env *e, int *dir, int *moves);
int					on_target(t_env *e);
int					get_dir(t_env *e, int *moves, int j, int *board);

/*
**	put.c
*/
void				put_end_states(t_env *e);
void				put_scene(t_env *e);

/*
**	error.c
*/
void				*testv(void *ret, void *err, char *where);
int					test(int ret, int err, char *where);
int					error(int ret, char *error);
void				die(int error, char *where);

#endif
