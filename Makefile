#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/05/12 18:06:35 by caupetit          #+#    #+#              #
#    Updated: 2014/06/01 15:04:12 by caupetit         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = lemipc
SRC = main.c \
	error.c signal.c env.c \
	shm.c sem.c \
	init_game.c game.c play.c alive.c \
	move.c dir.c target.c \
	put.c
CC = cc
FLAGS = -Wall -Wextra -Werror
LIB = libft/
INCLUDES = includes/
DIROBJ = objs/
DIRSRC = srcs/
OBJ = $(SRC:%.c=$(DIROBJ)%.o)

all: init $(NAME)

init:
	@make -s -C $(LIB)
	@tput init


$(NAME): $(OBJ)
	@echo "==> Compiling $(NAME)"
	@$(CC) $(FLAGS) -o $@ $(OBJ) -I $(INCLUDES) -L$(LIB) -lft
	@sleep .5
	@tput cuu1
	@echo "\033[2K\t\033[1;36m$(NAME)\t\t\033[0;32m[Ready]\033[0m"

$(DIROBJ)%.o: $(DIRSRC)%.c
	@echo "--> Compiling $<"
	@$(CC) $(FLAGS) -o $@ -c $< -I $(INCLUDES)

ipcs:
	@sh clear
	@echo "==> cleared ipcs"

clear: ipcs
	@rm -f `find **/*.[ch]~`
	@rm -f `find *~`
	@echo "==> cleared ~ files"

clean:
	@make -C libft $@
	@rm -f $(OBJ)
	@echo "[$(NAME)]--> Objects removed"

fclean: clean
	@make -C libft $@
	@rm -f $(NAME)
	@echo "[$(NAME)]--> Program removed"

re: fclean all
